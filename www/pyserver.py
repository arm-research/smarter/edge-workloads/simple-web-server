import http.server
import socketserver
import os
import socket


# Fetch env variables
SLEEP_TIME = float(os.getenv("SLEEP_TIME", 4))
MY_NODE_NAME = os.getenv("MY_NODE_NAME", "NODE_NAME")

os.chdir('/www/')

# update the index.html to include to hostname
input = open("index.html.template")
output = open("index.html", 'w')

for s in input:
    filled_template = s.replace("<HOSTNAME>", MY_NODE_NAME)
    filled_template = filled_template.replace("<SLEEP_TIME>", str(SLEEP_TIME * 1000))
    output.write(filled_template)

input.close()
output.close()

PORT = 8080
Handler = http.server.SimpleHTTPRequestHandler

with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()


    
